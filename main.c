/* Modern C - Challenge 12 - Text Processor
 * 
 * For a text processor, can you use a doubly linked list to store text?
 * The idea is to represent a "blob" of text through a struct that contains 
 * a string (for the text) and pointers to preceding and following blobs.
 * 
 * Can you build a function that splits a text blob in two at a given point?
 * One that joins two consecutive text blobs?
 * One that runs through the entire text and puts it in the form of one blob
 * per line?
 * Can you create a function that prints the entire text or prints until the 
 * text is cut off due to the screen size?
 */

#include <stdlib.h>
#include <stdio.h>

#define MAX_BUF_SZ 256

typedef struct node node;
struct node {
  char buf[MAX_BUF_SZ];
  node* prev;
  node* next;
};

void free_list(node*);

int main(int argc, char **argv){
  FILE* fp;
  if (argc != 2) 
  {
    printf("Error, no file!\nUsage %s [filename]\n", argv[0]);
    return -1;
  }
  else if ((fp = fopen(argv[1], "r")) == NULL){
    printf("Error, input must be of filetype!\nUsage %s [filename]\n", argv[0]);
    return -1;
  }

  node* head = malloc(sizeof(node));
  head->prev = NULL;
  head->next = NULL;

  node* nptr = head;

  if (fp != NULL){
    while (fread(nptr->buf, sizeof(nptr->buf) - 1, 1, fp) == 1 ){
      node *n = (node*) malloc(sizeof(node));
      n->prev = nptr;
      n->next = NULL;

      nptr->next = n;
      nptr->prev = nptr;
      nptr = n;
    }
  }

  fclose(fp);
  nptr = head; 
  
  /* print each node's text buffer to stdout */
  while (nptr != NULL){
    fputs(nptr->buf, stdout);
    nptr = nptr->next;
  }

  free_list(head);
  return 0;
}

void free_list(node* head){
  node* tmp;
  while (head != NULL){
    tmp = head;
    head = head->next;
    free(tmp);
  }
}
