CC=gcc
SRC=*main.c
OBJ=*.o
EXEC=ctp

ctp: $(OJB)
	$(CC) $(SRC) -o $(EXEC)

$(OBJ): $(SRC)
	$(CC) -c $(SRC)

.PHONY: clean
clean:
	rm $(EXEC)
