# C Text Processor

## Description
CTP is a commandline text processor written in C.
string data is stored into text buffers 
and connected by a doubly linked list. 

## Dependencies
* GCC compiler
* Make (optional)

## Installation
```
git clone <this repo> <your dir>
cd <your dir>
make
```
